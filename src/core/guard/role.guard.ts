import { Injectable, CanActivate, ExecutionContext, UnauthorizedException } from "@nestjs/common";
import { Reflector } from "@nestjs/core";
import { Observable } from "rxjs";
import { RoleUser } from "src/shared/constant/role.enum";
import { ROLES_KEY } from "src/shared/decorator/role.decorator";
import { TokenPayload } from "src/shared/interface/token-payload";

@Injectable()
export class RolesGuard implements CanActivate {
	constructor(private reflector: Reflector) {}

	canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
		const requiredRoles = this.reflector.get<RoleUser[]>(ROLES_KEY, context.getHandler());
		if (!requiredRoles) {
			return true;
		}
		const request = context.switchToHttp().getRequest();
		const user = request.user as TokenPayload;
		const isAuth = requiredRoles.some(role => role === user.role);
		if (!isAuth) {
			throw new UnauthorizedException("Error");
		}
		return isAuth;
	}

	handleRequest(err, user) {
		if (err || !user) {
			throw new UnauthorizedException("Error");
		}
		return user;
	}
}

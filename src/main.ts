import { NestFactory } from "@nestjs/core";
import { AppModule } from "./app.module";
import { SetupSwagger } from "./shared/swagger/swagger";
import { ValidationPipe } from "@nestjs/common";

async function bootstrap() {
	const app = await NestFactory.create(AppModule);
	app.useGlobalPipes(
		new ValidationPipe({
			transform: true,
		}),
	);
	SetupSwagger(app);
	await app.listen(3001);
}
bootstrap();

import { Module } from "@nestjs/common";
import { AppController } from "./app.controller";
import { AppService } from "./app.service";
import { ConfigModule, ConfigService } from "@nestjs/config";
import databaseConfig from "./databases/database.config";
import { TypeOrmModule } from "@nestjs/typeorm";
import { UserModule } from "./modules/users/users.module";
import { AuthModule } from "./modules/auth/auth.module";
import { AttachmentModule } from "./modules/attachment/attachment.module";

@Module({
	imports: [
		ConfigModule.forRoot({
			isGlobal: true,
			cache: true,
			load: [databaseConfig],
		}),
		TypeOrmModule.forRootAsync({
			inject: [ConfigService],
			useFactory: async (configService: ConfigService) => ({
				...configService.get("database"),
			}),
		}),
		UserModule,
		AuthModule,
		AttachmentModule,
	],
	controllers: [AppController],
	providers: [AppService],
})
export class AppModule {}

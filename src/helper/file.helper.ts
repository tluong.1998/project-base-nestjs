import { FileSizeUnit, ONE_KILOBYTE } from "../shared/constant/index";

export const getFileExtension = (fileName: string): string => {
	const parts = fileName.split(".");
	if (parts.length > 1) {
		return parts[parts.length - 1];
	} else {
		return "";
	}
};

export const convertFileSizeToByte = (size: number, unit: FileSizeUnit): number => {
	let times = 0;
	switch (unit) {
		case FileSizeUnit.KILOBYTE:
			times = 1;
			break;
		case FileSizeUnit.MEGABYTE:
			times = 2;
			break;
		case FileSizeUnit.GIGABYTE:
			times = 3;
			break;
		default:
			break;
	}
	return size * Math.pow(ONE_KILOBYTE, times);
};

import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { User } from "./user.entity";
import { AttachmentResponseDto } from "src/modules/attachment/dto/response.dto";

@Entity()
export class Attachment {
	@PrimaryGeneratedColumn()
	id: number;

	@Column()
	link: string;

	@Column()
	name: string;

	@Column()
	extension: string;

	@Column()
	createdTime: Date;

	@ManyToOne(() => User)
	@JoinColumn()
	createdBy: User;

	toDto(): AttachmentResponseDto {
		const attachmentDto = new AttachmentResponseDto();
		attachmentDto.id = this.id;
		attachmentDto.name = this.name;
		attachmentDto.link = this.link;
		attachmentDto.extension = this.extension;
		attachmentDto.createdTime = this.createdTime;
		attachmentDto.createdBy = this.createdBy?.id;
		return attachmentDto;
	}
}

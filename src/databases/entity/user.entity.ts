import {
	Entity,
	Column,
	PrimaryGeneratedColumn,
	OneToOne,
	JoinColumn,
	CreateDateColumn,
	UpdateDateColumn,
	DeleteDateColumn,
} from "typeorm";
import { Attachment } from "./attachment.entity";
import { Role } from "./role.entity";

@Entity()
export class User {
	@PrimaryGeneratedColumn()
	id: number;

	@Column({
		type: String,
		name: "name",
		default: "",
	})
	name: string;

	@Column({
		type: String,
		default: "",
		name: "phone",
	})
	phone: string;

	@Column({
		name: "isActive",
		type: Boolean,
		default: true,
	})
	isActive: boolean;

	@OneToOne(() => Role, { nullable: true })
	@JoinColumn({ name: "role" })
	role: Role;

	@Column({
		type: String,
		name: "email",
		// unique: true,
	})
	email: string;

	@Column({
		name: "password",
		type: String,
		select: false,
	})
	password: string;

	@OneToOne(() => Attachment)
	@JoinColumn()
	photo: Attachment;

	@CreateDateColumn()
	createdDate: Date;

	@UpdateDateColumn()
	updatedDate: Date;

	@DeleteDateColumn()
	deletedDate: Date;
}

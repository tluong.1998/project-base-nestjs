import { RoleUser } from "src/shared/constant/role.enum";
import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Role {
	@PrimaryGeneratedColumn()
	id: number;

	@Column()
	code: RoleUser;

	@Column()
	name: string;
}

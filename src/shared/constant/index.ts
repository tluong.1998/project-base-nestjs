export const LIMITED_FILE_SIZE = 5;
export const ONE_KILOBYTE = 1024;
export enum FileSizeUnit {
	BYTE,
	KILOBYTE,
	MEGABYTE,
	GIGABYTE,
}

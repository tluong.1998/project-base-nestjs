export const ResponseMessage = {
	unauthorized: "unauthorized",
	accountNotFound: "accountNotFound",
	wrongPassword: "wrongPassword",
	csvNotFound: "csvNotFound"
};

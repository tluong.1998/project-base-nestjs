import { ApiProperty } from "@nestjs/swagger";

export class BaseResponseDto {
	@ApiProperty({
		type: Number,
		default: 1,
	})
	id: number;

	@ApiProperty({
		type: "timestamptz",
		default: new Date(),
	})
	createdTime: Date;

	@ApiProperty({
		type: "timestamptz",
		default: new Date(),
	})
	updatedTime: Date;

	@ApiProperty({
		type: "timestamptz",
		default: new Date(),
	})
	deletedTime: Date;

	@ApiProperty({
		type: Number,
		default: 1,
	})
	createdBy: number;

	@ApiProperty({
		type: Number,
		default: 1,
	})
	updatedBy: number;

	@ApiProperty({
		type: Number,
		default: 1,
	})
	deletedBy: number;
}

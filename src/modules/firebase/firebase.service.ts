import { Injectable } from '@nestjs/common';
import * as admin from 'firebase-admin';

@Injectable()
export class FirebaseService {
  private readonly storage: admin.storage.Storage;
  constructor() {
    const serviceAccount = {
      type: process.env.FIREBASE_TYPE,
      project_id: process.env.FIREBASE_PROJECT_ID,
      private_key_id: process.env.FIREBASE_PRIVATE_KEY_ID,
      private_key: "-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQDaWF7c++48htm6\nBXvvH/+ekXueKwQom1PA/CgqlCrWwgkB9YNKq/hiTTSm0jZTms7IlqlbrBEIpux0\n6Ee5krLEnG5lGk0fhnSnnOa7qBsMszYDznNHyvcDP2j6FABSgaE3lhEJ/HeWlMTG\niIaSgDYD27L5+3ejK7ihY8Dl0A9tifVvuYeU1x6hQn/nt7nxPt55LSPnxwqQHBEa\nGzCa/OILtiFAke7dUmMoJ7bKROElGFk8g2vfgSgUe4vgG8kvf6tOwJ5fcNaJJsSK\nV+FpCqLwQ+oirDlo/BtziVppDav4FocLpaUEhl9g6LX7LUF4F92wXP+Wu1fMjw/Z\nZ9DIKLMDAgMBAAECggEAFUpNTbJJuPXT4HNNHLRRDNmAupvVLE8hATiqg08D8lbk\ni/Va7Pq3Hda2DezXcUWEvkMP69QWVnKjAdnttybvxC4WBvO5QMav30bJqiiqd+Li\n9HIQBEMX3OWKS0A3W9QvWn0z13ZvrsDTgyGoMDwiMt7G+osjpPHMGf3hFEePtDge\nYfNEz2LH5k+lyc1TxCA8Lg/U1N93Qu8clZStw4I/SYCJqAvHqwv8NMVFFrsNPzJ2\nvkKxvl5wEBYhkrw79Fi2c5fWRRBbLAzekJ+VXd9ACWzMtvsfrRkpW85e1Nx1fNn1\nOJJhaHwlyVL6nKP5ZsZzk5WE+y3DZ9B4AtpIU39JQQKBgQD3u+jNtTHb02ZmzAax\nd1YCe7jME8jh7+dI9Rf1ETgfETeCZ6EiAcBZ1iGGBdK0qh4Gk3/lDXyYFkilsOLQ\nJskegeE6x1GQOFeEkI1/LMV4eiplSvJi8ym0gfe4Bho32ANzOZX/Hw14m/BwHX+F\nROS8GQPuXTBGyDbmjJp1m6XrIwKBgQDhoW2c1InZHu8EGTQCGg7gu2TsjtK24MTO\nJxJm1gevKJkRQ3iCjWm2SOXk3bSHOIduhtMbTw2BYGfz8aTXadHaR0XKCG0/6pmm\nqGUf4IHEiKCmmIRUpFawuRgsCduvCre/+rmjkJ1+FfRxaeFbrXT6Ao8xbqI2Hddc\nRVNnoM8GoQKBgDTNv7yA/6Scdbo5iL+w8QtMdcJCAVJvQTwiyb15sx2kHNHV5tmJ\nyAceBj075oKNtCKws9UNv93m+gsfHelCKyIuiuRtcaws5jKiYRaXY2vflkPK6sk+\njUglX7jAzDvU9j9MKfWcF9H6YxByqSit25dgOZ9IC686o8EWfSHOUFpDAoGAAweX\n4TM5ZqDztKph+GVBLeNWR5bjXXFHMz1zEc5t6/wvkfvF8nrToI76B0iy093+c417\n6DH3jHUu8CeefwX8u2GZnkzeqq0LwyEfun3ZQDpVbec+X6SVRWo22OCJAqCyNvG2\n7GtUsgy5a4xwajq6B2FhatiBdRiRe7uykwGlxgECgYAGIm86BHj6nsFloVJKg/Mj\nvC02ck194z1ojMbXVwmDdk51PyDfhYIoKM9PhXP0+HNyGoJ4nVZlYHkbEeW5QYvK\n2Yci7Clx+JzEJ5PI8s40sBZ+Tyl010BshsHxrnXP3Dlwpa3QIb81S0SRgLPo+d2u\n7AA/v2KKhAeoyz4piDhyeQ==\n-----END PRIVATE KEY-----\n",
      client_email: process.env.FIREBASE_CLIENT_EMAIL,
      client_id: process.env.FIREBASE_CLIENT_ID,
      auth_uri: process.env.FIREBASE_AUTH_URI,
      token_uri: process.env.FIREBASE_TOKEN_URI,
      auth_provider_x509_cert_url: process.env.FIREBASE_AUTH_PROVIDER_X509_CERT_URL,
      client_x509_cert_url: process.env.FIREBASE_CLIENT_X509_CERT_URL,
      universe_domain: process.env.FIREBASE_UNIVERSE_DOMAIN,
    } as admin.ServiceAccount;
    
    admin.initializeApp({
      credential: admin.credential.cert(serviceAccount),
      storageBucket: `gs://${process.env.FIREBASE_PROJECT_ID}.appspot.com`,
    });
    this.storage = admin.storage();
  }

  getStorageInstance(): admin.storage.Storage {
    return this.storage;
  }
}

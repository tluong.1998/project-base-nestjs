import { ApiProperty } from "@nestjs/swagger";
import { IsEmail, IsNumber, IsString } from "class-validator";

export class RequestUser {
	@ApiProperty({
		type: String,
		default: "Duc luong",
	})
	@IsString()
	name: string;

	@ApiProperty({
		type: String,
		default: "0987654321",
	})
	@IsString()
	phone: string;

	@ApiProperty({
		type: Boolean,
		default: true,
	})
	isActive: boolean;

	@ApiProperty({
		type: Number,
		default: null,
	})
	@IsNumber()
	role: number;

	@ApiProperty({
		type: String,
		default: "Abc@gmail.com",
	})
	@IsEmail()
	email: string;

	@ApiProperty({
		type: Number,
		default: null,
		required: false,
	})
	@IsNumber()
	photo: number;
}

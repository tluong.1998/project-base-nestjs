import { Body, Controller, Get, Logger, Post, UseGuards } from "@nestjs/common";
import { UsersService } from "./users.service";
import { RolesGuard } from "src/core/guard/role.guard";
import { RoleUser } from "src/shared/constant/role.enum";
import { Roles } from "src/shared/decorator/role.decorator";
import { JwtGuard } from "src/core/guard/jwt.guard";
import { RequestUser } from "./interfaces/createUser";

/* eslint-disable */
const bcrypt = require("bcrypt");
@UseGuards(JwtGuard, RolesGuard)
@Controller("users")
export class UsersController {
	constructor(
		private userService: UsersService,
		private readonly logger: Logger,
	) {}

	@Roles(RoleUser.Admin)
	@Get("list")
	async getList() {
		try {
			return await this.userService.findAll();
		} catch (error) {
			this.logger.error(error, error.stack, UsersController.name);
			throw error;
		}
	}

	@Post("create")
	async create(@Body() body: RequestUser) {
		try {
			return await this.userService.create(body);
		} catch (error) {
			this.logger.error(error, error.stack, UsersController.name);
			throw error;
		}
	}
}

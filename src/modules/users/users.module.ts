import { Logger, Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { User } from "src/databases/entity/user.entity";
import { UsersService } from "./users.service";
import { UsersController } from "./users.controller";
import { PasswordService } from "../auth/services/password.service";

@Module({
	imports: [TypeOrmModule.forFeature([User])],
	controllers: [UsersController],
	providers: [UsersService, Logger, PasswordService],
	exports: [UsersService],
})
export class UserModule {}

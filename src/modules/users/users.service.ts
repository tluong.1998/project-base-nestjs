import { Injectable, NotFoundException } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { User } from "src/databases/entity/user.entity";
import { ResponseMessage } from "src/shared/constant/response-message";
import { Repository } from "typeorm";
import { RequestUser } from "./interfaces/createUser";
import { PasswordService } from "../auth/services/password.service";

@Injectable()
export class UsersService {
	constructor(
		@InjectRepository(User)
		private usersRepository: Repository<User>,
		private passworServices: PasswordService,
	) {}

	findAll(): Promise<User[]> {
		return this.usersRepository.find();
	}

	findOne(id: number): Promise<User | null> {
		return this.usersRepository.findOneBy({ id });
	}

	async create(body: RequestUser): Promise<User> {
		const { name, phone, isActive, role, photo, email } = body;

		const hashedPassword = await this.passworServices.hash(
			process.env.ACCOUNT_DEFAULT_PASSWORD,
		);
		const dataUser = this.usersRepository.create({
			name,
			phone,
			isActive,
			email,
			role: { id: role },
			photo: { id: photo },
			password: hashedPassword,
		});

		const saveUser = await this.usersRepository.save(dataUser, {
			reload: true,
		});
		return saveUser;
	}

	async remove(id: number): Promise<void> {
		await this.usersRepository.delete(id);
	}

	async findByEmail(email: string, options: object) {
		const foundAccount = await this.usersRepository.findOne({
			where: { email },
			relations: {
				role: true,
			},
			...options,
		});

		if (!foundAccount) {
			throw new NotFoundException(ResponseMessage.accountNotFound);
		}
		return foundAccount;
	}
}

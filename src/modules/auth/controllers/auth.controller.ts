import { Body, Controller, HttpCode, HttpStatus, Logger, Post } from "@nestjs/common";
import { LoginRequestDto } from "../dto/request/login.dto";
import { AuthService } from "../services/auth.service";
import { LoginResponseDto } from "../dto/response/login.dto";

@Controller("auth")
export class AuthController {
	constructor(
		private authService: AuthService,
		private readonly logger: Logger,
	) {}

	@HttpCode(HttpStatus.OK)
	@Post("login")
	async login(@Body() body: LoginRequestDto): Promise<LoginResponseDto> {
		try {
			return await this.authService.login(body);
		} catch (e) {
			this.logger.error(e, e.stack, AuthController.name);
			throw e;
		}
	}
}

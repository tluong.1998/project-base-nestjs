import { PassportStrategy } from "@nestjs/passport";
import { Injectable } from "@nestjs/common";
import { ExtractJwt, Strategy } from "passport-jwt";
import { TokenPayload } from "../interface/token-payload";

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy, "access-jwt") {
	constructor() {
		super({
			jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
			ignoreExpiration: false,
			secretOrKey: "JWT_SECRET_KEY",
		});
	}

	async validate(payload: TokenPayload) {
		const { id, code, role, email, tokenId, tokenCode }: TokenPayload = payload;
		return { id, code, role, email, tokenId, tokenCode } as TokenPayload;
	}
}

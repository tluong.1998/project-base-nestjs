import { BadRequestException, Injectable, NotFoundException } from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";
import { LoginRequestDto } from "../dto/request/login.dto";
import { LoginResponseDto } from "../dto/response/login.dto";
import { UsersService } from "src/modules/users/users.service";
import { TokenPayload } from "../interface/token-payload";
import { ResponseMessage } from "src/shared/constant/response-message";
import { PasswordService } from "./password.service";

@Injectable()
export class AuthService {
	constructor(
		private userService: UsersService,
		private jwtService: JwtService,
		private passwordService: PasswordService,
	) {}

	async login(body: LoginRequestDto) {
		const foundAccount = await this.userService.findByEmail(body.email, {
			select: {
				id: true,
				role: true,
				email: true,
				password: true,
			},
		});

		if (!foundAccount) {
			throw new NotFoundException(ResponseMessage.accountNotFound);
		}

		const validateAccount = await this.passwordService.compare(
			body.password,
			foundAccount.password,
		);

		if (!validateAccount) {
			throw new BadRequestException(ResponseMessage.wrongPassword);
		}

		const tokenPayload: TokenPayload = {
			id: foundAccount.id,
			email: foundAccount.email,
			role: foundAccount.role.code,
		};

		const accessToken = this.jwtService.sign(tokenPayload);
		return {
			accessToken,
		} as LoginResponseDto;
	}
}

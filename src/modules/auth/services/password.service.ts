import { Injectable } from "@nestjs/common";
import * as bcrypt from "bcrypt";

@Injectable()
export class PasswordService {
	async compare(password: string, hashedPassword: string) {
		return await bcrypt.compare(password, hashedPassword);
	}

	async hash(password: string) {
		return await bcrypt.hash(password, 10);
	}
}

import { RoleUser } from "src/shared/constant/role.enum";

export interface TokenPayload {
	id?: number;
	code?: string;
	email?: string;
	tokenId?: number;
	tokenCode?: string;
	role: RoleUser;
}

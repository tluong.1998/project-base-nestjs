import { ApiProperty } from "@nestjs/swagger";
import { IsString } from "class-validator";

export class LoginRequestDto {
	@ApiProperty({
		type: String,
		default: "example@gmail.com",
	})
	@IsString()
	readonly email: string;

	@ApiProperty({
		type: String,
		default: "aaAA11@@",
	})
	@IsString()
	readonly password: string;
}

import { ApiProperty } from "@nestjs/swagger";

export class LoginResponseDto {
	@ApiProperty({ default: "" })
	readonly accessToken: string;

	// @ApiProperty({ default: "" })
	// readonly refreshToken: string;
}

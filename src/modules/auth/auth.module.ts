import { Logger, Module } from "@nestjs/common";
import { AuthController } from "./controllers/auth.controller";
import { JwtModule } from "@nestjs/jwt";
import { AuthService } from "./services/auth.service";
import { UserModule } from "../users/users.module";
import { PasswordService } from "./services/password.service";
import { JwtStrategy } from "./strategy/jwt.strategy";

@Module({
	imports: [
		UserModule,
		JwtModule.register({
			global: true,
			secret: "JWT_SECRET_KEY",
			signOptions: { expiresIn: "1d" },
		}),
	],
	controllers: [AuthController],
	providers: [AuthService, Logger, PasswordService, JwtStrategy],
})
export class AuthModule {}

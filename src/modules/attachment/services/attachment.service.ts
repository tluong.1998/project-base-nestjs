import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { FirebaseService } from 'src/modules/firebase/firebase.service';
import { v4 as uuidv4 } from 'uuid';
import csvParser from 'csv-parser';
import * as fs from 'fs';
import { Test } from '../interface/test.interface';
import { ResponseMessage } from 'src/shared/constant/response-message';
import { InjectRepository } from '@nestjs/typeorm';
import { Attachment } from 'src/databases/entity/attachment.entity';
import { Repository } from 'typeorm';

@Injectable()
export class AttachmentService {
  constructor(
    private firebaseService: FirebaseService,
    @InjectRepository(Attachment) private attachmentRepository: Repository<Attachment>,
  ) {}

  async uploadImage(files: Express.Multer.File[]) {
    console.log('files', files);
    const createAttachments: Attachment[] = [];
    if (files) {
      createAttachments.push();
    }

    for (const file of files) {
      const storage = this.firebaseService.getStorageInstance();
      const bucket = storage.bucket();
      const uuid = uuidv4();
      const fileName = `${Date.now()}_${file.originalname}`;
      const fileUpload = bucket.file(fileName);
      const stream = fileUpload.createWriteStream({
        metadata: {
          contentType: file.mimetype,
          metadata: {
            firebaseStorageDownloadTokens: uuid,
          },
        },
        public: true,
      });

      const fileImage: unknown = new Promise((resolve, rejects) => {
        stream.on('error', (error) => {
          rejects(error);
        });
  
        stream.on('finish', () => {
          const imageUrl = `https://storage.googleapis.com/${bucket.name}/${fileName}`;
          resolve(imageUrl);
        });
  
        stream.end(file.buffer);
      });

      console.log('fimeImage', fileImage)
    }

    

    this.attachmentRepository.create({
      // link: fileImage,
    });

    return 'success';
  }

  async uploadCSV(file_csv) {
    console.log('file path', file_csv.path);

    if (!file_csv) {
      throw new NotFoundException(ResponseMessage.csvNotFound);
    }

    // Check if the uploaded file is a CSV file
    if (file_csv.mimetype !== 'text/csv') {
      throw new BadRequestException('Uploaded file is not a CSV');
    }

    if (!file_csv.path) {
      throw new BadRequestException('Uploaded file is not a CSV 123');
    }

    const results: Test[] = [];

    fs.createReadStream(file_csv.path)
      .pipe(csvParser())
      .on('data', (data: Test) =>
        results.push({
          ...data,
        }),
      )
      .on('end', () => console.log('resuilt', results))
      .on('error', (error) => error);

    return 'Success';
  }
}

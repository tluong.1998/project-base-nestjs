import { ApiProperty } from "@nestjs/swagger";
import { BaseResponseDto } from "src/shared/dto/base.response.dto";

export class AttachmentResponseDto extends BaseResponseDto {
	@ApiProperty({
		type: String,
		default: "avatar.png",
	})
	name: string;

	@ApiProperty({
		type: String,
		default: "png",
	})
	extension: string;

	@ApiProperty({
		type: String,
		default: "https://avatar.png",
	})
	link: string;
}

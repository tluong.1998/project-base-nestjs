export interface Test {
    Username: string,
    Identifier: number,
    Firstname: string,
    Lastname: string
} 
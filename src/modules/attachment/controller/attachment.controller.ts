import {
  Controller,
  HttpStatus,
  Logger,
  ParseFilePipeBuilder,
  Post,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { JwtGuard } from 'src/core/guard/jwt.guard';
import { convertFileSizeToByte } from 'src/helper/file.helper';
import { FileSizeUnit, LIMITED_FILE_SIZE } from 'src/shared/constant';
import { AttachmentService } from '../services/attachment.service';

@Controller('attachment')
export class AttachmentController {
  constructor(
    private attachmentService: AttachmentService,
    private readonly logger: Logger,
  ) {}
  @UseGuards(JwtGuard)
  @Post('upload-image')
  @UseInterceptors(FileInterceptor('file'))
  async uploadFile(
    @UploadedFile(
      new ParseFilePipeBuilder()
        .addMaxSizeValidator({
          maxSize: convertFileSizeToByte(LIMITED_FILE_SIZE, FileSizeUnit.MEGABYTE),
        })
        .addFileTypeValidator({
          fileType: /(jpg|jpeg|png|gif)$/,
        })
        .build({
          errorHttpStatusCode: HttpStatus.BAD_REQUEST,
        }),
    )
    files: Express.Multer.File[],
  ) {
    try {
      const attachmentFile = await this.attachmentService.uploadImage(files);
      return attachmentFile;
    } catch (e) {
      this.logger.error(e, e.stack, AttachmentController.name);
      throw e;
    }
  }

  @Post('upload-csv')
  @UseInterceptors(FileInterceptor('file_csv'))
  async uploadCsv(@UploadedFile() files_csv: Express.Multer.File) {
    try {
      const uploadCSV = await this.attachmentService.uploadCSV(files_csv);
      return uploadCSV;
    } catch (e) {
      this.logger.error(e, e.stack, AttachmentController.name);
      throw e;
    }
  }
}

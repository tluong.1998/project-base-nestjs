import { Logger, Module } from "@nestjs/common";
import { AttachmentController } from "./controller/attachment.controller";
import { AttachmentService } from "./services/attachment.service";
import { FirebaseService } from "../firebase/firebase.service";
import { Attachment } from "src/databases/entity/attachment.entity";
import { TypeOrmModule } from "@nestjs/typeorm";

@Module({
	imports: [
		TypeOrmModule.forFeature([Attachment])
	],
	controllers: [AttachmentController],
	providers: [AttachmentService, Logger, FirebaseService],
})
export class AttachmentModule {}
